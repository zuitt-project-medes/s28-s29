// First, we load the expressjs module into our application and saved it in a variable called express
const express = require('express');

// Create an application with expressjs.
// This creates an application that uses express and stores it as app
// app is our server
const app = express();

// port is a variable that contains the port number we want to designate to our server
const port = 4000;

// middleware
// express.json() is a method from express which allow us to handle the streaming of data and automatically parse the incoming JSON from our req body.
// app.use is used to run a method or another function for our expressjs api
app.use(express.json());

// mock data
let users = [
	{
		username: "BMadrigal",
		email: "fateReader@gmail.com",
		password: "weDontTalkAboutMe"
	},
	{
		username: "LuisaMadrigal",
		email: "strongSis@gmail.com",
		password: "pressure"
	}
];

let items = [
	{
		name: "roses",
		price: 170,
		isActive: true
	},
	{
		name: "tulips",
		price: 250,
		isActive: true
	}
];

// Express has a methods to use as routes corresponding to HTTP methods.
// app.get(<endpoint>, <function handling req and res)

app.get('/', (req, res) => {

	// Once the route is accessed, we can send a response with the use of res.send()
	// res.send() actually combines writeHead() and end().
		// used to send a response to the client and ends the request
	res.send('Hello from ExpressJS Api!')
});

/*
	Mini Activity

	>> Create a get route in Expressjs which will be able to send a message in the client:

		>> endpoint: /greeting
		>> message: 'Hello from Batch169-surname'

	>> Test in postman
	>> Send your response in hangouts

*/

app.get('/greeting', (req, res) => {

	res.send('Hello from Batch169-Medes')
});

app.get('/users', (req, res) => {

	// res.send() stringifies it for you
	res.send(users);
});

// How do we get data from the client as a request body?

app.post('/users', (req, res) => {

	console.log(req.body)

	let reqBody = req.body

	let newUser = {

		username: reqBody.username,
		email: reqBody.email,
		password: reqBody.password
	}

	users.push(newUser);
	console.log(users);

	res.send(users);
})


app.delete('/users', (req, res) => {

	users.pop();
	console.log(users);

	res.send(users);
});

// updating users route

app.put('/users/:index', (req, res) => {

	// req.body- this will contain the updated password
	console.log(req.body);

	// req.params object which contains the value in the url params
	// url params is captured by route parameter (:parameterName) and saved as property in req.params
	console.log(req.params);

	// parseInt the value of the number coming from req.params
	let index = parseInt(req.params.index);

	// get the user that we want to update with our index number from url params
	users[index].password = req.body.password

	// send the updated user to the client
	// provide the index variable to be the index for the particular item in the array
	res.send(users[index]);
});

app.get('/users/getSingleUser/:index', (req, res) => {

	let index = parseInt(req.params.index);

	res.send(users[index]);
});

/*
	Mini Activity

	>> Create a new route to get and send items array in the client

	>> Create a new route to create and add a new item object in the items array
		>> send the updated items array in the client
		>> check the post method route for our users for reference

	>> Create a new route which can update the price of a single item in the array
		>> pass the index number of the item that you want to update in the request body
		>> add the index number of the item you want to update to access it and its price property
		>> reassign the new price from our request body
		>> send the updated item in the client

	>> Send your Postman outputs in hangouts

*/

app.get('/items', (req, res) => {

	res.send(items);
});


app.post('/items', (req, res) => {

	console.log(req.body)

	let reqBody = req.body


	let newItem = {

		name: reqBody.name,
		price: reqBody.price,
		isActive: reqBody.isActive
	}

	items.push(newItem);

	res.send(items);
});

app.put('/items/:index', (req, res) => {

	let index = parseInt(req.params.index);

	items[index].price = req.body.price;

	res.send(items[index]);
});


app.get('/items/getSingleItem/:index', (req, res) => {

	let index = parseInt(req.params.index);

	res.send(items[index]);
});




// A C T I V I T Y
/*
>> Create a new route with '/items/getSingleItem/:index' endpoint. 
	>> This route should allow us to GET the details of a single item.
		-Pass the index number of the item you want to get via the url as url params in your postman.
		-http://localhost:4000/items/getSingleItem/<indexNumberOfItem>
	>> Send the request and back to your api/express:
		-get the data from the url params.
		-send the particular item from our array using its index to the client.

>> Create a new route to update an item with '/items/archive/:index' endpoint.	
	>> This route should allow us to UPDATE the isActive property of our product.
		-Pass the index number of the item you want to de-activate via the url as url params.
		-Access the particular item with its index. Access the isActive property of the item and re-assign it to false.
		-send the updated item in the client

>> Create a new route to update an item with '/items/activate/:index' endpoint. 
	>> This route should allow us to UPDATE the isActive property of our product.
		-Pass the index number of the item you want to activate via the url as url params.
		-Access the particular item with its index. Access the isActive property of the item and re-assign it to true.
		-send the updated item in the client

>> Save the Postman collection in your s28-s29 folder

*/

// S28-S29 ACTIVITY

app.get("/items/getSingleUser/:index" , (req, res) => {
	let index = parseInt(req.params.index);
	res.send(items[index]);
});

app.put("/items/archive/:index", (req, res) => {

	let index = parseInt(req.params.index);

	items[index].isActive = req.body.isActive

	res.send(items[index]);
});

app.put("/items/activate/:index", (req, res) => {

	let index = parseInt(req.params.index);

	items[index].isActive = req.body.isActive

	res.send(items[index]);
});


// listen method, server listens to the assigned port
app.listen(port, () => console.log(`Server is running at port ${port}`))
